import React from 'react';
import logo from './logo.svg';
import './App.css';
import Title from './Title';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Title children="Hello Sandeep" />
        <p>
          Edit <code>src/App.js</code> and save to reload. check hello
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
